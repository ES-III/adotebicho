<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Animal;

class AnimalController extends Controller
{
    public function index(){

        $animals = Animal::orderBy('id','tipo')->paginate(8);
        //$animals = Animal::all();
        return view('animaisList', compact('animals'));
        //return $animals;
    }

    public function create(){
        $acao = 1;
        return view('animaisCad', compact('acao'));

    }

    public function store(Request $request){
      
        $this->validate($request, [
            'tipo' => 'required|min:2|max:200',
            'raca' => 'required',
            'porte' => 'required',
            'idade' => 'required',
            'historico' => 'required',
            'temperamento' => 'required',
             
        ]);

        Animal::create($request->all());
        return redirect()->route('animais.index')->with('status', ' Cadastro efetuado com sucesso!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ClienteController extends Controller
{
    public function __construct() {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function index() {
       // if($clientes = User::where('ativo', 0)->orderBy('name')->paginate(5)){
         //  return view('clientes', compact('clientes'));
         return view('clientes');  
      //  } 
    }

    public function indexjson() {
           return User::where('ativo', 0)->paginate(10);        
    }*/

    public function index(Request $request){
        $clientes = User::where('ativo', 0)->orderBy('name')->paginate(5);                  
        if ($request->ajax()) {
            return view('componentes.tabela-clientes', array('clientes' => $clientes))->render();
        }       
        return view('clientes', compact('clientes'));
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

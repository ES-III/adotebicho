<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Contatosuser;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }

    public function perfil($id) {
        $usuario = User::find($id);           
        $contato = Contatosuser::find($id);
        if(isset($contato)){           
            return view('perfil', compact('usuario'));
        }else{
            $contato = new Contatosuser();
            $contato->user_id = $id;
            $contato->save();            
            return view('perfil', compact('usuario'));
        }                      
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $usuario = User::find($id);       
        //VALIDACAO DOS DADOS        
        $request->validate([
            'name' => 'required|string|min:3|max:30', 
            'email' => 'required|string|email',
            'celular1' => 'required', 'cep' => 'required', 
            'rua' => 'required|string|max:50', 'numero' => 'required|max:10',
            'complemento' => 'max:30', 'bairro' => 'required|string|max:30', 
            'cidade' => 'required|string|max:30', 'uf' => 'required'
        ], [                                    
            'required' => 'O campo :attribute é requerido!',  
            'name.required' => 'O campo nome é requerido!', 
            'celular1.required' => 'O campo celular é requerido!',     
            'cep.required' => 'O campo CEP é requerido!',
            'uf.required' => 'O campo UF é requerido!',                      
            'min' => 'O campo :attribute é muito curto!',
            'max' => 'O campo :attribute é muito longo!',   
            'email.email' => 'O e-mail não é válido!',
            'numeric' => 'O campo :attribute não é válido'          
        ]);
        //ALTERA OS DADOS DO CLIENTE E SEUS CONTATOS
        $usuario->name = $request->input('name');
        $usuario->email = $request->input('email');  
        $foto = $request->file('foto');      
        if(isset($foto)){            
            if($usuario->foto === "imagens/perfil.png"){
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            }else {
                Storage::disk('public')->delete($usuario->foto);      
                $usuario->foto = $request->file('foto')->store('imagens', 'public');
            }                  
        }              
        $usuario->save();
        $contatos = Contatosuser::find($id);
        $contatos->cep = $request->input('cep');
        $contatos->rua = $request->input('rua');
        $contatos->numero = $request->input('numero');
        $contatos->complemento = $request->input('complemento');
        $contatos->bairro = $request->input('bairro');
        $contatos->cidade = $request->input('cidade');
        $contatos->uf = $request->input('uf');
        $contatos->celular1 = $request->input('celular1');
        $contatos->celular2 = $request->input('celular2');
        $contatos->residencial = $request->input('residencial');
        $contatos->facebook = $request->input('facebook');
        $contatos->twitter = $request->input('twitter');
        $contatos->instagram = $request->input('instagram'); 
        $contatos->save();     
        $request->session()->flash('OK', ' DADOS ATUALIZADOS COM SUCESSO!'); 
        return redirect('home/perfil/' . $id);
    }
}

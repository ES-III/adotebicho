<?php

use Illuminate\Database\Seeder;

class AnimaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('animals')->insert([
            'tipo' => 'Cachorro',
            'raca' => 'Pastor Alemao',
            'porte' => 'Medio',
            'idade' => '5',
            'historico' => 'Morou toda vida com os primeiros donos',
            'temperamento' => 'Docil',
        ]);

        DB::table('animals')->insert([
            'tipo' => 'Gato',
            'raca' => 'Vira-Lata',
            'porte' => 'Pequeno',
            'idade' => '3',
            'historico' => 'Achado perdido a 1 ano atras',
            'temperamento' => 'Docil',
        ]);
    }
}

// PARTE DO DASHBOARD
$(function () {
    "use strict";

    $(".preloader").fadeOut();
    // this is for close icon when navigation open in mobile view
    $(".nav-toggler").on('click', function () {
        $("#main-wrapper").toggleClass("show-sidebar");
        $(".nav-toggler i").toggleClass("ti-menu");
    });
    $(".search-box a, .search-box .app-search .srh-btn").on('click', function () {
        $(".app-search").toggle(200);
        $(".app-search input").focus();
    });

    // ============================================================== 
    // Resize all elements
    // ============================================================== 
    $("body, .page-wrapper").trigger("resize");
    $(".page-wrapper").delay(20).show();

    //****************************
    /* This is for the mini-sidebar if width is less then 1170*/
    //**************************** 
    var setsidebartype = function () {
        var width = (window.innerWidth > 0) ? window.innerWidth : this.screen.width;
        if (width < 1170) {
            $("#main-wrapper").attr("data-sidebartype", "mini-sidebar");
        } else {
            $("#main-wrapper").attr("data-sidebartype", "full");
        }
    };
    $(window).ready(setsidebartype);
    $(window).on("resize", setsidebartype);
});
// FIM DA PARTE DO DASHBOARD

// PARTE QUE FAZ APARECER OS BALOESZINHOS PRETOS NOS LINKS
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
// FIM DOS BALOESZINHOS

// SCRIPT QUE COLOCA AS MENSAGENS DE RETORNO DOS METODOS NA VIEW
$(function () {
    var jElement = $('.alert');

    $(document).ready(function () {
        if ($(this).scrollTop() >= 0) {
            jElement.css({
                'position': 'fixed',
                'bottom': '5%',
                'right': '3%'
            });
        }
    });
});

// SCRIPT QUE COLOCA AS MASCARAS NOS INPUTS (PRIMEIRO JEITO)
/**$(document).ready(function(){
    $(":input").inputmask();
    or
    Inputmask().mask(document.querySelectorAll("input"));
});*/
// FIM DO SCRIPT QUE COLOCA AS MASCARAS NOS INPUTS (PRIMEIRO JEITO)

// SCRIPT QUE COLOCA AS MASCARAS NOS INPUTS (SEGUNDO JEITO)
$(document).ready(function () {
    //specifying options 
    $("#celular1").inputmask({
        "mask": "(99) 99999-9999"
    });
    $("#celular2").inputmask({
        "mask": "(99) 99999-9999"
    });
    $("#residencial").inputmask({
        "mask": "(99) 9999-9999"
    });
    $("#cep").inputmask({
        "mask": "99999-999"
    });
    $("#uf").inputmask({
        "mask": "AA"
    });
});
// FIM DO SCRIPT QUE COLOCA AS MASCARAS NOS INPUTS (SEGUNDO JEITO)

// SCRIPT QUE USA A API DE BUSCA DE CEP TUDO VIA AJAX
$(document).ready(function () {
    $('#cep').blur(function () {
        $.ajax({
            url: 'http://cep.republicavirtual.com.br/web_cep.php?cep=' + $('#cep').val() + '&formato=json',
            dataType: 'json',
            success: function (data) {
                if (data.resultado === "1") {
                    $('#rua').val(data.logradouro);
                    $('#cidade').val(data.cidade);
                    $('#bairro').val(data.bairro);
                    $('#uf').val(data.uf);
                    $('#numero').focus();
                }
            }
        });
    });
});
// FIM DO SCRIPT DE BUSCA DE CEP

// PAGINACAO
$(function () {
    $('body').on('click', '.pagination a', function (e) {
        e.preventDefault();
        //$('#load a').css('color', '#dfecf6');
        //$('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');
        var url = $(this).attr('href');
        getArticles(url);
        window.history.pushState("", "", url);
    });

    function getArticles(url) {
        $.ajax({
            url: url
        }).done(function (data) {
            $('.users').html(data);
        }).fail(function () {
            alert('Articles could not be loaded.');
        });
    }
});
// FIM DA PAGINACAO

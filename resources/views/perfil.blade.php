@extends('layouts.dashboard')
@section('conteudo')
<!-- PARTE DO CONTEUDO EM SI -->         
<div class="page-wrapper">
<!-- BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" --> 
<div class="page-breadcrumb">
   <div class="row align-items-center">
      <div class="col-5">
         <h4 class="page-title">PERFIL</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                  @if($usuario->perfil === "CLIENTE")
                  <a href="\home">Home (Dashboard)</a></li>
                  @else
                  <a href="\admin">Home (Dashboard)</a></li>
                  @endif
                  <li class="breadcrumb-item active" aria-current="page">Perfil</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="col-7">
         <div class="text-right upgrade-btn">
            <a href="#" class="btn btn-danger text-white">
            <i class="mdi mdi-settings"></i> CONFIGURAÇÕES</a>
         </div>
      </div>
   </div>
</div>
<!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
<!-- CONTEUDO FLUIDO  --> 
<div class="container-fluid">
   <!-- LINHA -->
   <div class="row">
      <!-- COLUNA DA ESQUERDA (PERFIL) -->
      <div class="col-lg-4 col-xlg-3 col-md-5">
         <div class="card">
            <div class="card-body">
               <div class="m-t-30 text-center">
                  <img src="/storage/{{$usuario->foto}}" 
                     class="rounded-circle" width="150" height="150" />
                  <h4 class="card-title m-t-10">{{ $usuario->name }}</h4>
                  @if($usuario->perfil === "CLIENTE")
                  <h6 class="card-subtitle">Cliente usuário do sistema</h6>
                  <div class="row text-center justify-content-md-center">
                     <div class="col-12">{{$usuario->pontos}} pontos</div>                     
                  </div>
                  @else
                  <h6 class="card-subtitle">Administrador do sistema</h6>
                  @endif
               </div>
            </div>
            <div>
               <hr>
            </div>
            <div class="card-body">
               <small class="text-muted">ENDEREÇO DE E-MAIL</small>
               <h6>{{$usuario->email}}</h6>
               <small class="text-muted p-t-30 db">CELULAR/WHATSAPP</small>
               <h6>{{$usuario->contato->celular1}}</h6>
               <small class="text-muted p-t-30 db">CELULAR 2</small>
               <h6>{{$usuario->contato->celular2}}</h6>
               <small class="text-muted p-t-30 db">TELEFONE RESIDENCIAL</small>
               <h6>{{$usuario->contato->residencial}}</h6>
               <small class="text-muted p-t-30 db">ENDEREÇO</small>
               <h6>{{$usuario->contato->rua}}@if(isset($usuario->contato->numero)), n° {{$usuario->contato->numero}} @else @endif 
                  @if(isset($usuario->contato->complemento))— {{$usuario->contato->complemento}} @else @endif
               </h6>
               <small class="text-muted p-t-30 db">BAIRRO</small>
               <h6>{{$usuario->contato->bairro}}</h6>
               <small class="text-muted p-t-30 db">CEP</small>
               <h6>{{$usuario->contato->cep}}</h6>
               <small class="text-muted p-t-30 db">CIDADE (UF)</small>
               <h6>{{$usuario->contato->cidade}}@if(isset($usuario->contato->uf)) ({{$usuario->contato->uf}}) @else @endif</h6>
               <div class="map-box">
                  <small class="text-muted p-t-30 db">MAPA DO ENDEREÇO</small>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3392.087307743616!2d-52.362735284843325!3d-31.768104581288352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9511b580b410e105%3A0xb0f0c0688e4e847e!2sR.+Mal.+Setembrino+de+Carvalho%2C+104+-+Fragata%2C+Pelotas+-+RS%2C+96030-390!5e0!3m2!1spt-BR!2sbr!4v1540058910979"  
                     width="100%" 
                     height="150" 
                     frameborder="0" 
                     style="border:0" 
                     allowfullscreen>
                  </iframe>
               </div>
               <small class="text-muted p-t-30 db">REDES SOCIAIS</small>
               <br/> 
               @if(isset($usuario->contato->facebook))
               <a href="{{$usuario->contato->facebook}}" target="_blank" 
                  class="btn btn-circle btn-secondary link-rede-sociais"
                  data-toggle="tooltip" data-placement="top" title="Facebook">
               <i class="fab fa-facebook-f"></i></a>
               @else                  
               @endif
               @if(isset($usuario->contato->twitter))
               <a href="{{$usuario->contato->twitter}}" target="_blank"
                  class="btn btn-circle btn-secondary link-rede-sociais"
                  data-toggle="tooltip" data-placement="top" title="Twitter">
               <i class="fab fa-twitter"></i></a>
               @else              
               @endif
               @if(isset($usuario->contato->instagram))
               <a href="{{$usuario->contato->instagram}}" target="_blank" 
                  class="btn btn-circle btn-secondary link-rede-sociais"
                  data-toggle="tooltip" data-placement="top" title="Instagram">
               <i class="fab fa-instagram"></i></a>
               @else               
               @endif
            </div>
         </div>
      </div>
      <!-- FIM DA COLUNA DA ESQUERDA (PERFIL) -->
      <!-- COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL) -->
      <div class="col-lg-8 col-xlg-9 col-md-7">
         <div class="card">
            <div class="card-header titulo-card">
               <h5>ATUALIZAR O CADASTRO (PERFIL)</h5>
               <div class="form-requerido">
                  <small><em>* O CAMPO É OBRIGATÓRIO!</em></small>
               </div>
            </div>
            <div class="card-body">
               <form method="POST" novalidate enctype="multipart/form-data"
               @if($usuario->perfil === "CLIENTE") 
               action="/home/{{$usuario->id}}" 
               @else 
               action="/admin/{{$usuario->id}}" 
               @endif
               class="form-horizontal form-material">
               @csrf
               <div class="form-group">              
                  <label for="name"><strong>Nome completo</strong>
                  <span class="form-requerido">*</span></label>
                  <input id="name" type="text" 
                     class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-sm"
                     name="name" value="{{$usuario->name}}" 
                     placeholder="Digite seu nome completo" />
                  @if ($errors->has('name'))                             
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('name') }}</strong>                                    
                  </span>
                  @endif                   
               </div>
               <div class="form-group">   
                  <label for="email"><strong>E-mail</strong> 
                  <span class="form-requerido">*</span></label>
                  <input id="email" type="email" 
                     class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-sm"
                     name="email" value="{{$usuario->email}}" 
                     placeholder="Digite seu e-mail" />
                  @if ($errors->has('email'))                             
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('email') }}</strong>                                    
                  </span>
                  @endif                    
               </div>
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label for="celular1"><strong>Celular/WhatsApp</strong> 
                     <span class="form-requerido">*</span></label>
                     <input id="celular1" type="text" 
                        class="form-control{{ $errors->has('celular1') ? ' is-invalid' : '' }} form-control-sm" 
                        name="celular1" value="{{$usuario->contato->celular1}}"
                        placeholder="Digite seu celular/WhatsApp" />
                     @if ($errors->has('celular1'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('celular1') }}</strong>                                    
                     </span>
                     @endif
                  </div>
                  <div class="form-group col-md-6">
                     <label for="celular2"><strong>Celular 2</strong></label>
                     <input id="celular2" type="text" 
                        class="form-control form-control-sm" 
                        name="celular2" value="{{$usuario->contato->celular2}}" 
                        placeholder="Digite seu outro celular"/>                    
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-8">
                     <label for="residencial">
                     <strong>Telefone residencial</strong>
                     </label>
                     <input id="residencial" type="text" 
                        class="form-control form-control-sm" 
                        name="residencial" value="{{$usuario->contato->residencial}}" 
                        placeholder="Digite seu telefone" />                                       
                  </div>
                  <div class="form-group col-md-4">
                     <label for="cep"><strong>CEP</strong>
                     <span class="form-requerido">*</span></label>                    
                     <input id="cep" type="text" 
                        class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }} form-control-sm" 
                        name="cep" value="{{$usuario->contato->cep}}" 
                        placeholder="CEP"/>
                     @if ($errors->has('cep'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('cep') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-8">
                     <label for="rua"><strong>Rua</strong>
                     <span class="form-requerido">*</span></label>                    
                     <input id="rua" type="text" 
                        class="form-control{{ $errors->has('rua') ? ' is-invalid' : '' }} form-control-sm" 
                        name="rua" value="{{$usuario->contato->rua}}" 
                        placeholder="Digite sua rua" />
                     @if ($errors->has('rua'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('rua') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
                  <div class="form-group col-md-4">
                     <label for="numero"><strong>Número</strong>
                     <span class="form-requerido">*</span></label>                    
                     <input id="numero" type="number" 
                        class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }} form-control-sm" 
                        name="numero" value="{{$usuario->contato->numero}}" 
                        placeholder="N.°" />
                     @if ($errors->has('numero'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('numero') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-7">   
                     <label for="complemento"><strong>Complemento</strong></label>
                     <input id="complemento" type="text" 
                        class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }} form-control-sm"
                        name="complemento" value="{{$usuario->contato->complemento}}"
                        placeholder="Digite seu complemento" />
                     @if ($errors->has('complemento'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('complemento') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
                  <div class="form-group col-md-5">   
                     <label for="bairro"><strong>Bairro</strong>
                     <span class="form-requerido">*</span></label>
                     <input id="bairro" type="text" 
                        class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }} form-control-sm"
                        name="bairro" value="{{$usuario->contato->bairro}}"
                        placeholder="Digite seu bairro" />
                     @if ($errors->has('bairro'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('bairro') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-9">
                     <label for="cidade"><strong>Cidade</strong>
                     <span class="form-requerido">*</span></label>                    
                     <input id="cidade" type="text" 
                        class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }} form-control-sm" 
                        name="cidade" value="{{$usuario->contato->cidade}}"
                        placeholder="Digite sua cidade" />
                     @if ($errors->has('cidade'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('cidade') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
                  <div class="form-group col-md-3">
                     <label for="uf"><strong>UF</strong>
                     <span class="form-requerido">*</span></label>                    
                     <input id="uf" type="text" 
                        class="form-control{{ $errors->has('uf') ? ' is-invalid' : '' }} form-control-sm" 
                        name="uf" value="{{$usuario->contato->uf}}"
                        placeholder="UF" />
                     @if ($errors->has('uf'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('uf') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
               </div>
               <div class="form-group">              
                  <label for="foto"><strong>Foto (Perfil)</strong>
                  <span class="aviso-senha">· ideal 150 x 150 pixels</span></label>
                  <input id="foto" type="file" name="foto"
                     class="form-control-file form-control-sm" />                                      
               </div>
               <div class="form-group">              
                  <label for="facebook"><strong>Facebook</strong></label>
                  <input id="facebook" type="text" 
                     class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }} form-control-sm"
                     name="facebook" value="{{$usuario->contato->facebook}}"
                     placeholder="Digite a URL do seu Facebook" />
                  @if ($errors->has('facebook'))                             
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('facebook') }}</strong>                                    
                  </span>
                  @endif                   
               </div>
               <div class="form-group">              
                  <label for="twitter"><strong>Twitter</strong></label>
                  <input id="twitter" type="text" 
                     class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }} form-control-sm"
                     name="twitter" value="{{$usuario->contato->twitter}}"
                     placeholder="Digite a URL do seu Twitter" />
                  @if ($errors->has('twitter'))                             
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('twitter') }}</strong>                                    
                  </span>
                  @endif                   
               </div>
               <div class="form-group">              
                  <label for="instagram"><strong>Instagram</strong></label>
                  <input id="instagram" type="text" 
                     class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }} form-control-sm"
                     name="instagram" value="{{$usuario->contato->instagram}}"
                     placeholder="Digite a URL do seu Instagram" />
                  @if ($errors->has('instagram'))                             
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('instagram') }}</strong>                                    
                  </span>
                  @endif                   
               </div>
               <div class="form-group">
                  <div class="col-sm-12">
                     <button class="btn btn-success btn-espaco" type="submit">
                     <i class="mdi mdi-cached"></i> ATUALIZAR PERFIL</button>                    
                     @if($usuario->perfil === "CLIENTE") 
                     <a href="/home" 
                        class="btn btn-secondary btn-espaco">
                     <i class="mdi mdi-close"></i> CANCELAR</a>
                     @else 
                     <a href="/admin" 
                        class="btn btn-secondary btn-espaco">
                     <i class="mdi mdi-close"></i> CANCELAR</a>
                     @endif                                     
                  </div>
               </div>
               </form>
            </div>
         </div>
      </div>
      @if (session('OK'))   
      <div class="alert alerta-sucesso alert-dismissible" role="alert">
         <i class="fas fa-check-circle"></i>{{ session('OK') }}          
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
         <span aria-hidden="true">&times;</span>
         </button>
      </div>
      @endif
      <!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->    
   </div>
   <!-- FIM DA LINHA -->   
</div>
<!-- FIM DO CONTEUDO FLUIDO  --> 
<!-- AQUI TERIA Q TER UMA </div> PRA FECHAR A PARTE DO CONTEUDO EM SI
   MAS ELA ESTA NA PARTE DO LAYOUT DO DASHBOARD -->  
@endsection
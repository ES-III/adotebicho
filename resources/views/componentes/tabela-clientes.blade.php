<table class="table v-middle text-nowrap">
   <thead>
      <tr class="bg-light">
         <th class="border-top-0">FOTO</th>
         <th class="border-top-0">NOME</th>
         <th class="border-top-0">E-MAIL</th>
         <th class="border-top-0">ENDEREÇO</th>
         <th class="border-top-0">BAIRRO</th>
         <th class="border-top-0">CELULAR</th>
         <th class="border-top-0 text-center">AÇÕES</th>
      </tr>
   </thead>
<tbody>
@foreach($clientes as $c)
      <tr>
         <td><img src="/storage/{{$c->foto}}" 
            class="rounded-circle" width="40" height="40" /></td>
         <td>{{$c->name}}</td>
         <td>{{$c->email}}</td>
         <td>{{$c->contato->rua}}, n° {{$c->contato->numero}}                                         
            @if(isset($c->contato->complemento))
            — {{$c->contato->complemento}}
            @else                                            
            @endif
         </td>
         <td>{{$c->contato->bairro}}</td>
         <td>{{$c->contato->celular1}}</td>
         <td class="text-center">
            <a href="" class="link-tabela"><i class="mdi mdi-pencil"></i></a>&nbsp;                                          
            <a href="" class="link-tabela"><i class="mdi mdi-block-helper"></i></a>                                           
         </td>
      </tr>
      @endforeach  
   </tbody>
</table>
<hr>
<div class="paginacao">
   {{$clientes->links()}}
</div>
<!doctype html>
<html>
   <head>
      <title>Cadastro - AdoteDog</title>
      <meta charset="UTF-8">
      <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <!-- Favicon (ícone na aba do navegador) -->
      <link rel="icon" href="{!! asset('favicon.ico') !!}" type="image/x-icon"/>
      <link rel="shortcut icon" type="image/x-icon" href="{!! asset('favicon.ico') !!}" />
      <!-- Estilos (Bootstrap e personalizado) -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">     
   </head>
   <body>
      <div id="root" class="fundo-cadastre-se">
         <main class="cr-app">
            <div class="cr-content container-fluid">
               <div class="row form-center">
                  <div class="col-md-7 col-lg-5" style="margin: auto">
                     <div class="card card-body">
                        <form method="POST" action="{{ route('register') }}" novalidate>
                           @csrf
                           <div class="text-center pb-4">
                              <a href="/">
                              <img src="{!! asset('img/logo-animal.png') !!}" 
                                 alt="logo" class="margem-baixo-logo-cad-login" width="100" height="100" />
                              </a>
                              <h5><strong>BEM VINDO!</strong></h5>
                              <h5><strong>FAÇA O SEU CADASTRO!</strong></h5>
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label for="name">Nome <span class="form-requerido">*</span></label>
                                 <input id="name" type="text" name="name"
                                 class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-sm" 
                                 value="{{ old('name') }}" required autofocus placeholder="Digite seu nome">
                                 @if ($errors->has('name'))                             
                                 <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>                                    
                                 </span>
                                 @endif
                              </div>
                              <div class="form-group col-md-6">
                                 <label for="sobrenome">Sobrenome <span class="form-requerido">*</span></label>
                                 <input id="sobrenome" type="text" name="sobrenome"
                                    class="form-control{{ $errors->has('sobrenome') ? ' is-invalid' : '' }} form-control-sm" 
                                    value="{{ old('sobrenome') }}" required placeholder="Digite seu sobrenome">
                                 @if ($errors->has('sobrenome'))                             
                                 <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sobrenome') }}</strong>                                    
                                 </span>
                                 @endif
                              </div>
                           </div>
                           <div class="form-group">
                              <label for="email">E-mail <span class="form-requerido">*</span></label>                            
                              <input id="email" type="email" name="email" 
                                 class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-sm" 
                                 value="{{ old('email') }}" required placeholder="Digite seu e-mail">
                              @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                 <strong>{{ $errors->first('email') }}</strong>
                              </span>
                              @endif                           
                           </div>
                           <div class="form-row">
                              <div class="form-group col-md-6">
                                 <label for="password">Senha <span class="form-requerido">*</span>
                                 <span class="aviso-senha">· 6 digitos ou maior</span></label>
                                 <input id="password" type="password" 
                                    class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-sm" 
                                    name="password" required placeholder="Digite sua senha">
                                 @if ($errors->has('password'))
                                 <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                 </span>
                                 @endif
                              </div>
                              <div class="form-group col-md-6">
                                 <label for="password-confirm">Confirme sua senha <span class="form-requerido">*</span></label>                           
                                 <input id="password-confirm" type="password" name="password_confirmation"  
                                    class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} form-control-sm" 
                                    required placeholder="Digite a senha novamente"> 
                                 @if ($errors->has('password_confirmation'))
                                 <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>                                  
                                 </span>
                                 @endif                          
                              </div>
                           </div>
                           <div class="row">
                               <div class="col-md-6"><button type="submit" 
                                class="border-0  btn btn-dark btn-block">CADASTRAR</button></div>
                            <div class="col-md-6">
                                    <button type="reset" 
                                    class="border-0  btn btn-warning btn-block">LIMPAR</button>
                            </div>
                            
                           </div>
                          
                           <div class="form-requerido text-center">
                               <small><strong><em>* O CAMPO É OBRIGATÓRIO!</em></strong></small>
                           </div>
                           <hr>
                           <div class="text-center pt-1">
                              <small>
                              <strong>OU</strong> <a href="/login"><strong>FAZER LOGIN?</strong></a> | 
                              <a href="/"><i class="fas fa-home"></i> <strong>VOLTAR</strong></a>
                              </small>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </main>
      </div>
   </body>
</html>
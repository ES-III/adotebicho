<!doctype html>
<html dir="ltr" lang="en">
   <head>
      <title>Dashboard AdoteDog</title>
      <meta charset="UTF-8">
      <!--<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      
      
      <!-- Estilos (Bootstrap e personalizado) -->
      <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">     
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- ESTILO DO TODO (PRINCIPAL) -->     
      <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" 
         data-sidebartype="full" data-sidebar-position="absolute" 
         data-header-position="absolute" data-boxed-layout="full">
         <!-- MENU SUPERIOR (CABECALHO) -->         
         <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
               <div class="navbar-header" data-logobg="skin5">
                  <!-- LOGO DO CANTO SUPERIOR ESQUERDO (MENU SUPERIOR) -->                     
                  <a class="navbar-brand" href="/">
                     <!-- LOGO -->
                     <b class="logo-icon">
                     <img src="{!! asset('img/logo-dashboard.png') !!}" 
                        class="light-logo" alt="homepage" />
                     </b> 
                     <!-- TEXTO DO LADO DO LOGO -->                        
                     <span class="logo-text">                             
                     <img src="{!! asset('img/razao-social.png') !!}" 
                        class="light-logo" alt="homepage" />
                     </span>
                  </a>
                  <!-- FIM DO LOGO -->                     
                  <!-- BOTAO QUE MOSTRA O MENU LATERAL, VISIVEL SOMENTE NO CELULAR!!! -->
                  <a class="nav-toggler waves-effect waves-light d-block d-md-none" 
                     href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
               </div>
               <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                  <ul class="navbar-nav float-left mr-auto">
                     <!-- LUPA (PROCURA) Q FICA NO MENU SUPERIOR (CABECALHO) DO LADO ESQUERDO -->                         
                     <li class="nav-item search-box">
                        <!--<a class="nav-link waves-effect waves-dark" 
                           href="javascript:void(0)"><i class="fas fa-search"></i></a>
                        <form class="app-search position-absolute">
                           <input type="text" class="form-control" 
                              placeholder="Procurar &amp; enter"> <a class="srh-btn">
                           <i class="ti-close"></i></a>
                        </form>-->
                     </li>
                  </ul>
                  <!-- IMG DE PERFIL (MENU) DO LADO DIREITO DO MENU SUPERIOR (CABECALHO) -->                     
                  <ul class="navbar-nav float-right">
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" 
                           href="" data-toggle="dropdown" aria-haspopup="true" 
                           aria-expanded="false"><img src="/storage/{{ Auth::user()->foto }}" 
                           alt="user" class="rounded-circle" width="31"></a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated">
                           @if(Auth::user()->perfil === "CLIENTE")
                           <a class="dropdown-item" href="/home/perfil/{{Auth::user()->id}}">
                           @else
                           <a class="dropdown-item" href="/admin/perfil/{{Auth::user()->id}}">
                           @endif                           
                           <i class="ti-user m-r-5 m-l-5"></i> MEU PERFIL</a>
                           <a class="dropdown-item" href="javascript:void(0)">
                           <i class="ti-wallet m-r-5 m-l-5"></i> MEU BALANÇO</a>
                           <a class="dropdown-item" href="javascript:void(0)">
                           <i class="ti-email m-r-5 m-l-5"></i> CAIXA DE ENTRADA</a>
                           <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                 <i class="fa fa-power-off m-r-5 m-l-5"></i> SAIR</a>
                                 <form id="logout-form" action="{{ route('logout') }}" 
                                    method="POST" style="display: none;">
                                    @csrf
                           </form>
                        </div>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!-- FIM DO MENU A PARTIR DA IMG DE PERFIL Q FICA A DIREITA NO MENU SUPERIOR (CABECALHO) -->        
         <!-- MENU LATERAL ESQUERDO  -->         
         <aside class="left-sidebar" data-sidebarbg="skin6">
            <div class="scroll-sidebar">
               <nav class="sidebar-nav">
                  <ul id="sidebarnav" class="in">
                     <!-- PRIMEIRO ITEM DO MENU LATERAL (PERFIL DO USUARIO)-->                      
                     <li>
                        <div class="user-profile d-flex no-block dropdown m-t-20">
                           <div class="user-pic"><img src="/storage/{{ Auth::user()->foto }}" 
                              alt="users" class="rounded-circle" width="40" /></div>
                           <div class="user-content hide-menu m-l-10">
                              <a href="javascript:void(0)" class="" id="Userdd" 
                                 role="button" data-toggle="dropdown" 
                                 aria-haspopup="true" aria-expanded="false">
                                 <h5 class="m-b-0 user-name font-medium">
                                    {{ Auth::user()->name }} <i class="fa fa-angle-down"></i>
                                 </h5>
                                 <span class="op-5 user-email" 
                                 title="{{ Auth::user()->email }}">{{ Auth::user()->email }}</span>
                              </a>
                              <div class="dropdown-menu dropdown-menu-right" 
                                 aria-labelledby="Userdd">
                                 @if(Auth::user()->perfil === "CLIENTE")
                                 <a class="dropdown-item"href="/home/perfil/{{Auth::user()->id}}">
                                 @else
                                 <a class="dropdown-item" href="/admin/perfil/{{Auth::user()->id}}">
                                 @endif
                                 <i class="ti-user m-r-5 m-l-5"></i> MEU PERFIL</a>
                                 <a class="dropdown-item" href="javascript:void(0)">
                                 <i class="ti-wallet m-r-5 m-l-5"></i> MEU BALANÇO</a>
                                 <a class="dropdown-item" href="javascript:void(0)">
                                 <i class="ti-email m-r-5 m-l-5"></i> CAIXA DE ENTRADA</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="javascript:void(0)">
                                 <i class="ti-settings m-r-5 m-l-5"></i> CONFIGURAÇÕES</a>
                                 <div class="dropdown-divider"></div>
                                 <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                 <i class="fa fa-power-off m-r-5 m-l-5"></i> SAIR</a>
                                 <form id="logout-form" action="{{ route('logout') }}" 
                                    method="POST" style="display: none;">
                                    @csrf
                                 </form>
                              </div>
                           </div>
                        </div>
                        <!-- FIM DO PERFIL DO USUARIO DO MENU LATERAL ESQUERDO -->
                     </li>
                     <li class="p-15 m-t-10"><a href="javascript:void(0)" 
                        class="btn btn-block create-btn text-white no-block d-flex align-items-center">
                        <i class="fa fa-plus-square"></i> <span class="hide-menu m-l-5">NOVO PEDIDO</span></a>
                     </li>
                     <!-- ITENS DO MENU LATERAL ESQUERDO -->
                     <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="index.html" aria-expanded="false">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span class="hide-menu">Dashboard</span></a>
                     </li>
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="/admin/clientes" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Clientes</span></a>
                     </li>
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="table-basic.html" aria-expanded="false">
                        <i class="mdi mdi-food"></i>
                        <span class="hide-menu">Lanches</span></a>
                     </li>
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="icon-material.html" aria-expanded="false">
                        <i class="mdi mdi-truck"></i>
                        <span class="hide-menu">Fornecedores</span></a>
                     </li>
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="error-404.html" aria-expanded="false">
                        <i class="mdi mdi-package-variant-closed"></i>
                        <span class="hide-menu">Estoque</span></a>
                     </li>
                     <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" 
                           href="error-404.html" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Usuários</span></a>
                     </li>                    
                  </ul>
               </nav>
            </div>
         </aside>
         <!-- FIM DO MENU LATERAL ESQUERDO -->
         @hasSection('conteudo')
         @yield('conteudo')
         @endif
         <!-- footer -->
         <footer class="footer text-center">
            All Rights Reserved by Xtreme Admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
         </footer>
         <!-- End footer -->
      </div>
      <!-- End Page wrapper  -->
      </div>
      <!-- Bootstrap core JavaScript -->
      <script src="{{ asset('js/app.js') }}"></script>  
      <!--Wave Effects -->
      <script src="{{ asset('js/waves.js') }}"></script>
      <!--Menu sidebar -->
      <script src="{{ asset('js/sidebarmenu.js') }}"></script>   
      <!-- MASCARAS DOS CAMPOS INPUTS -->
      <script src="{{ asset('js/jquery.inputmask.bundle.js') }}"></script>   
      <!--Custom JavaScript -->                
      <script src="{{ asset('js/custom.js') }}"></script>     
   </body>
</html>
@extends('layouts.dashboard')
@section('conteudo')
<!-- PARTE DO CONTEUDO EM SI -->         
<div class="page-wrapper">
<!-- BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" --> 
<div class="page-breadcrumb">
   <div class="row align-items-center">
      <div class="col-5">
         <h4 class="page-title">PERFIL</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Perfil</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="col-7">
         <div class="text-right upgrade-btn">
            <a href="#" class="btn btn-danger text-white">
                <i class="fas fa-cog"></i> CONFIGURAÇÕES</a>
         </div>
      </div>
   </div>
</div>
<!-- FIM DA BARRA DE CAMINHO (ONDE ESTOU?) E BTN DE "NOVO AVISO" -->
<!-- CONTEUDO FLUIDO  --> 
<div class="container-fluid">
   <!-- LINHA -->
   <div class="row">
      <!-- COLUNA DA ESQUERDA (PERFIL) -->
      <div class="col-lg-4 col-xlg-3 col-md-5">
         <div class="card">
            <div class="card-body">
               <div class="m-t-30 text-center">
                  <img src="{!! asset('img/perfil.jpg') !!}" 
                     class="rounded-circle" width="150" />
                  <h4 class="card-title m-t-10">{{ $user->name }}</h4>
                  <h6 class="card-subtitle">Cliente do sistema</h6>
               </div>
            </div>
            <div>
               <hr>
            </div>
            <div class="card-body">
               <small class="text-muted">ENDEREÇO DE E-MAIL</small>
               <h6>{{$user->email}}</h6>
               <small class="text-muted p-t-30 db">WHATSAPP</small>
               <h6>{{$user->contato->whatsapp}}</h6>
               <small class="text-muted p-t-30 db">CELULAR</small>
               <h6>{{$user->contato->celular}}</h6>
               <small class="text-muted p-t-30 db">TELEFONE RESIDENCIAL</small>
               <h6>{{$user->contato->residencial}}</h6>
               <small class="text-muted p-t-30 db">ENDEREÇO</small>
               <h6>{{$user->contato->rua}}@if(isset($user->contato->numero)), n° {!! $user->contato->numero !!}@endif</h6>
               <small class="text-muted p-t-30 db">COMPLEMENTO</small>
               <h6>{{$user->contato->complemento}}</h6>
               <small class="text-muted p-t-30 db">BAIRRO</small>
               <h6>{{$user->contato->bairro}}</h6>
               <small class="text-muted p-t-30 db">CEP</small>
               <h6>{{$user->contato->cep}}</h6>
               <small class="text-muted p-t-30 db">CIDADE (UF)</small>
               <h6>{{$user->contato->cidade}}
               @if(isset($user->contato->uf))({!! $user->contato->uf !!})@endif</h6>
               <div class="map-box">
                  <small class="text-muted p-t-30 db">MAPA DO ENDEREÇO</small>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3392.087307743616!2d-52.362735284843325!3d-31.768104581288352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9511b580b410e105%3A0xb0f0c0688e4e847e!2sR.+Mal.+Setembrino+de+Carvalho%2C+104+-+Fragata%2C+Pelotas+-+RS%2C+96030-390!5e0!3m2!1spt-BR!2sbr!4v1540058910979"  
                     width="100%" 
                     height="150" 
                     frameborder="0" 
                     style="border:0" 
                     allowfullscreen>
                  </iframe>
               </div>
               <small class="text-muted p-t-30 db">REDES SOCIAIS</small>
               <br/> 
               @if($user->contato->facebook === '' || $user->contato->facebook === null)
               @else             
               <a href="{{$user->contato->facebook}}" target="_blank" 
                   class="btn btn-circle btn-secondary link-rede-sociais">
               <i class="fab fa-facebook-f"></i></a>
               @endif
               @if($user->contato->twitter === '' || $user->contato->twitter === null)
               @else
               <a href="{{$user->contato->twitter}}" target="_blank"
                   class="btn btn-circle btn-secondary link-rede-sociais">
               <i class="fab fa-twitter"></i></a>
               @endif
               @if($user->contato->instagram === '' || $user->contato->instagram === null)
               @else
               <a href="{{$user->contato->instagram}}" target="_blank" 
                   class="btn btn-circle btn-secondary link-rede-sociais">
               <i class="fab fa-instagram"></i></a>
               @endif
            </div>
         </div>
      </div>
      <!-- FIM DA COLUNA DA ESQUERDA (PERFIL) -->
      <!-- COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL) -->
      <div class="col-lg-8 col-xlg-9 col-md-7">         
         <div class="card">
         <div class="card-header titulo-card">
           <h5>ATUALIZE O CADASTRO</h5>
        </div>
            <div class="card-body">
               <form method="POST" action="/home/{{$user->id}}" 
                     class="form-horizontal form-material" novalidate>
                  @csrf
                  <div class="form-group">              
                     <label for="name"><strong>Nome completo</strong></label>
                     <input id="name" type="text" 
                        class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-line"
                        name="name" value="{{$user->name}}" />
                     @if ($errors->has('name'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('name') }}</strong>                                    
                     </span>
                     @endif                   
                  </div>
                  <div class="form-group">   
                     <label for="email"><strong>E-mail</strong></label>
                     <input id="email" type="email" 
                        class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-line"
                        name="email" value="{{$user->email}}" />
                     @if ($errors->has('email'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('email') }}</strong>                                    
                     </span>
                     @endif                    
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="whatsapp"><strong>WhatsApp</strong></label>
                        <input id="whatsapp" type="text" 
                           class="form-control{{ $errors->has('whatsapp') ? ' is-invalid' : '' }} form-control-line" 
                           name="whatsapp" value="{{$user->contato->whatsapp}}" />                           
                        @if ($errors->has('whatsapp'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('whatsapp') }}</strong>                                    
                        </span>
                        @endif
                     </div>
                     <div class="form-group col-md-6">
                        <label for="celular"><strong>Celular</strong></label>
                        <input id="celular" type="text" 
                           class="form-control{{ $errors->has('celular') ? ' is-invalid' : '' }} form-control-line" 
                           name="celular" value="{{$user->contato->celular}}" />
                        @if ($errors->has('celular'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('celular') }}</strong>                                    
                        </span>
                        @endif
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-6">
                        <label for="residencial">
                            <strong>Telefone residencial</strong>
                        </label>
                        <input id="residencial" type="text" 
                           class="form-control{{ $errors->has('residencial') ? ' is-invalid' : '' }} form-control-line" 
                           name="residencial" value="{{$user->contato->residencial}}" />
                        @if ($errors->has('residencial'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('residencial') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-6">
                        <label for="cep"><strong>CEP</strong></label>                    
                        <input id="cep" type="text" 
                           class="form-control{{ $errors->has('cep') ? ' is-invalid' : '' }} form-control-line" 
                           name="cep" value="{{$user->contato->cep}}" />
                        @if ($errors->has('cep'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cep') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-10">
                        <label for="rua"><strong>Rua</strong></label>                    
                        <input id="rua" type="text" 
                           class="form-control{{ $errors->has('rua') ? ' is-invalid' : '' }} form-control-line" 
                           name="rua" value="{{$user->contato->rua}}" />
                        @if ($errors->has('rua'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('rua') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-2">
                        <label for="numero"><strong>Numero</strong></label>                    
                        <input id="numero" type="number" 
                           class="form-control{{ $errors->has('numero') ? ' is-invalid' : '' }} form-control-line" 
                           name="numero" value="{{$user->contato->numero}}" />
                        @if ($errors->has('numero'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('numero') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-6">   
                        <label for="complemento"><strong>Complemento</strong></label>
                        <input id="complemento" type="text" 
                           class="form-control{{ $errors->has('complemento') ? ' is-invalid' : '' }} form-control-line"
                           name="complemento" value="{{$user->contato->complemento}}" />
                        @if ($errors->has('complemento'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('complemento') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-6">   
                        <label for="bairro"><strong>Bairro</strong></label>
                        <input id="bairro" type="text" 
                           class="form-control{{ $errors->has('bairro') ? ' is-invalid' : '' }} form-control-line"
                           name="bairro" value="{{$user->contato->bairro}}" />
                        @if ($errors->has('bairro'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('bairro') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-row">
                     <div class="form-group col-md-10">
                        <label for="cidade"><strong>Cidade</strong></label>                    
                        <input id="cidade" type="text" 
                           class="form-control{{ $errors->has('cidade') ? ' is-invalid' : '' }} form-control-line" 
                           name="cidade" value="{{$user->contato->cidade}}" />
                        @if ($errors->has('cidade'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('cidade') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                     <div class="form-group col-md-2">
                        <label for="uf"><strong>UF</strong></label>                    
                        <input id="uf" type="text" 
                           class="form-control{{ $errors->has('uf') ? ' is-invalid' : '' }} form-control-line" 
                           name="uf" value="{{$user->contato->uf}}" />
                        @if ($errors->has('uf'))                             
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('uf') }}</strong>                                    
                        </span>
                        @endif                    
                     </div>
                  </div>
                  <div class="form-group">              
                     <label for="facebook"><strong>Facebook (URL)</strong></label>
                     <input id="facebook" type="text" 
                        class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }} form-control-line"
                        name="facebook" value="{{$user->contato->facebook}}">
                     @if ($errors->has('facebook'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('facebook') }}</strong>                                    
                     </span>
                     @endif                   
                  </div>
                  <div class="form-group">              
                     <label for="twitter"><strong>Twitter (URL)</strong></label>
                     <input id="twitter" type="text" 
                        class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }} form-control-line"
                        name="twitter" value="{{$user->contato->twitter}}" />
                     @if ($errors->has('twitter'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('twitter') }}</strong>                                    
                     </span>
                     @endif                   
                  </div>
                  <div class="form-group">              
                     <label for="instagram"><strong>Instagram (URL)</strong></label>
                     <input id="instagram" type="text" 
                        class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }} form-control-line"
                        name="instagram" value="{{$user->contato->instagram}}" />
                     @if ($errors->has('instagram'))                             
                     <span class="invalid-feedback" role="alert">
                     <strong>{{ $errors->first('instagram') }}</strong>                                    
                     </span>
                     @endif                   
                  </div>
                  <div class="form-group">
                     <div class="col-sm-12">
                        <button class="btn btn-success"
                           type="submit"><i class="fas fa-sync-alt"></i> ATUALIZAR PERFIL</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!-- FIM DA COLUNA DA DIREITA (FORMULARIO DE EDICAO DO PERFIL)-->    
   </div>
   <!-- FIM DA LINHA -->   
</div>
<!-- FIM DO CONTEUDO FLUIDO  --> 
<!-- AQUI TERIA Q TER UMA </div> PRA FECHAR A PARTE DO CONTEUDO EM SI
   MAS ELA ESTA NA PARTE DO LAYOUT DO DASHBOARD -->  
@endsection
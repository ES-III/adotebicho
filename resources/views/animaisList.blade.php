@extends('layouts.app')

@section('title', 'Animais Cadastrados')

@section('content_header')
    <h1>Animais Cadastrados</h1>
@endsection

@section('content')

@if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>  
@endif

<table class="table table-striped">
  <tr>
    <th> Id </th>
    <th> Tipo </th>
    <th> Raça </th>
    <th> Porte </th>
    <th> Idade </th>
    <th> Histórico </th>
    <th> Temperamento </th>    
  </tr>  
@forelse($animals  as $a)
  <tr>
    <td> {{$a->id}} </td>
    <td> {{$a->tipo}} </td>
    <td> {{$a->raca}} </td>
    <td> {{$a->porte}} </td>
    <td> {{$a->idade}} </td>
    <td> {{$a->historico}} </td>
    <td> {{$a->temperamento}} </td>
    
  </tr>
@empty
  <tr><td colspan=8> Não existem clientes Cadastrados No sistema </td></tr>
@endforelse
</table>
@endsection

@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection
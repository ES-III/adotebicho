@extends('layouts.app')

@section('content')
<div class="container">
  <form method="POST" action="{{route('animais.store')}}"> 
    {!! csrf_field() !!}
    <h3 class="lead">Cadastrar animal pra doação</h3>   
    <br>
    
    <div class="row">
      <div class="form-group col-sm-12 col-md-6">
        <label for="nome">Nome</label>
        <input class="form-control" type="text" name="nome" id="nome" required>
      </div>
      <div class="form-group col-sm-12 col-md-6">
        <label for="raca">Raça</label>
        <input class="form-control" type="text" name="raca" id="raca" required>
      </div>
    </div>
    
    <div class="row">
        <div class="form-group col-sm-12 col-md-6">
          <label for="porte">Porte</label>
          <input class="form-control" type="text" name="porte" id="porte" required>
        </div>
        <div class="form-group col-sm-12 col-md-6">
          <label for="idade">Idade</label>
          <input class="form-control" type="text" name="idade" id="idade" required>
        </div>
      </div>

      <div class="row">
          <div class="form-group col-sm-12 col-md-6">
            <label for="temperamento">Temperamento</label>
            <input class="form-control" type="text" name="temperamento" id="temperamento" required>
          </div>
      </div>
      
      <div class="row">
        <div class="form-group col-sm-12">
          <label for="historico">Historico</label>
          <textarea class="form-control" name="historico" id="historico" cols="30" rows="10"></textarea>
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12 text-center align-center">
          <button class="btn btn-primary btn-lg" type="submit">Doar</button>
          <button class="white-text btn btn-warning btn-lg" type="reset">Limpar</button>
        </div>
      </div>
  </form>
</div>
@endsection

    
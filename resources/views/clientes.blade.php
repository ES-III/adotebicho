@extends('layouts.dashboard')
    <!-- FIM DO MENU LATERAL ESQUERDO -->
@section('conteudo')
        <div class="page-wrapper">                     
            <div class="page-breadcrumb">
                <div class="row align-items-center">
                    <div class="col-5">
                        <h4 class="page-title">CLIENTES</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item active" aria-current="page"><a href="/admin">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Clientes</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                    <div class="col-7">
                        <div class="text-right upgrade-btn">
                            <a href="#" class="btn btn-success text-white"><i class="fa fa-plus-square"></i> NOVO CLIENTE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
            <div class="row">                  
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">                             
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">LISTA DOS CLIENTES ATIVOS</h4>
                                        <h5 class="card-subtitle">Exebindo {{$clientes->count()}} 
                                            clientes de {{$clientes->total()}} 
                                            ({{$clientes->firstItem()}} a {{$clientes->lastItem()}}).</h5>
                                    </div>
                                    <div class="ml-auto">
                                        <div class="dl">
                                            <select class="custom-select">
                                                <option value="0" selected>Monthly</option>
                                                <option value="1">Daily</option>
                                                <option value="2">Weekly</option>
                                                <option value="3">Yearly</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>                               
                            </div>
                            <div class="table-responsive users">

   
                                  
                                    
                                    @include('componentes.tabela-clientes')                                  
                                                                     
                               
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-md-flex align-items-center">
                                    <div>
                                        <h4 class="card-title">Sales Summary</h4>
                                        <h5 class="card-subtitle">Overview of Latest Month</h5>
                                    </div>
                                    <div class="ml-auto d-flex no-block align-items-center">
                                        <ul class="list-inline font-12 dl m-r-15 m-b-0">
                                            <li class="list-inline-item text-info"><i class="fa fa-circle"></i> Iphone</li>
                                            <li class="list-inline-item text-primary"><i class="fa fa-circle"></i> Ipad</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-lg-12">
                                        <div class="campaign ct-charts"></div>
                                    </div>                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Feeds</h4>
                                <div class="feed-widget">
                                    <ul class="list-style-none feed-body m-0 p-b-20">
                                        <li class="feed-item">
                                            <div class="feed-icon bg-info"><i class="far fa-bell"></i></div> You have 4 pending tasks. <span class="ml-auto font-12 text-muted">Just Now</span></li>
                                        <li class="feed-item">
                                            <div class="feed-icon bg-success"><i class="ti-server"></i></div> Server #1 overloaded.<span class="ml-auto font-12 text-muted">2 Hours ago</span></li>
                                        <li class="feed-item">
                                            <div class="feed-icon bg-warning"><i class="ti-shopping-cart"></i></div> New order received.<span class="ml-auto font-12 text-muted">31 May</span></li>
                                        <li class="feed-item">
                                            <div class="feed-icon bg-danger"><i class="ti-user"></i></div> New user registered.<span class="ml-auto font-12 text-muted">30 May</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>         
        </div>
     
    
        @endsection
